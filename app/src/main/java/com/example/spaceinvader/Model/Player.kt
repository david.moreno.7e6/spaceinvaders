package com.example.spaceinvader.Model

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.RectF
import com.example.spaceinvader.R

class Player(context: Context, screenX: Int, screenY: Int) {
    var bitmap: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.nave)
    val horizontal= screenX
    val vertical= screenY
    val width = screenX / 6f
    val height = screenY / 10f
    var positionX = screenX / 2
    var speed = 0
    var lifes= 3
    val hitbox= RectF()

    init{
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(),false)
    }

    fun updatePlayer(){
        if (positionX+speed > horizontal-200 ){
            positionX = horizontal-200
        }
        else if (positionX+speed < 0){
            positionX=0
        }
        else{
            positionX += speed
        }
    }
}