package com.example.spaceinvader.Model

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.RectF
import com.example.spaceinvader.R

class EnemyShot(context: Context, screenX: Int, screenY: Int, enemyType: Int, score: Int){
    var typeOfShots= arrayOf(
        BitmapFactory.decodeResource(context.resources, R.drawable.gusanoshot),
        BitmapFactory.decodeResource(context.resources, R.drawable.nazi),
        BitmapFactory.decodeResource(context.resources, R.drawable.espada),
        BitmapFactory.decodeResource(context.resources, R.drawable.chuletashot),
        BitmapFactory.decodeResource(context.resources, R.drawable.engranajeshot)
    )
    var bitmap: Bitmap = typeOfShots[enemyType]
    var width = screenX / 20f
    var height = screenY / 10f
    var positionX = screenX/2f
    var positionY= 180f
    var speed = 20
    val hitbox= RectF()

    init{
        when(enemyType){
            0->{
                speed=40
                width = 150f
                height = 250f
            }
            1->{
                speed=40

                width = 100f
                height = 100f
            }
            2->{
                speed=80

                width = 80f
                height = 220f
            }
            3->{
                speed=40

                width = 150f
                height =200f
            }
            4->{
                speed=30
                width = 300f
                height = 300f
            }

        }
        when(score){
            in 1500..2500-> speed+=10
            in 2600..3500-> speed+=20
            in 3600..5000-> speed+=30
            in 5100..10000-> speed+=40
            in 10100..25000->speed+=50
            in 25100..200000-> speed+=60
        }
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(),false)
    }

    fun updateShot(numX: Float){
        positionX= numX
        positionY+=speed
    }
}