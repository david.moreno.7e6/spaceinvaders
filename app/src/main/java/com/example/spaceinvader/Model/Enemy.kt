package com.example.spaceinvader.Model

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.RectF
import com.example.spaceinvader.R
import kotlin.properties.Delegates
class Enemy(context: Context, screenX: Int, screenY: Int) {
    val enemies= arrayOf(
        BitmapFactory.decodeResource(context.resources, R.drawable.gusano),
        BitmapFactory.decodeResource(context.resources, R.drawable.linconln),
        BitmapFactory.decodeResource(context.resources, R.drawable.terry),
        BitmapFactory.decodeResource(context.resources, R.drawable.chuleta),
        BitmapFactory.decodeResource(context.resources, R.drawable.engranaje))
    var numero:Int = 0
    var bitmap: Bitmap
    val horizontal= screenX
    var width = screenX / 5f
    var height = screenY / 10f
    var positionX = screenX / 2
    var alive= true
    var right= true
    var left= false
    var type by Delegates.notNull<Int>()
    var speed=10
    val hitbox= RectF()

    init{
        numero= (0..enemies.lastIndex).random()
        bitmap= enemies[numero]
        type= numero
        when(type){
            0,4->{
                speed=10
            }
            1,2->{speed=20}
            3->{speed=20}

        }
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(),false)
    }

    fun updateEnemy(){
        if (right){
            if (positionX+speed > horizontal-200 ){
                positionX = horizontal-200
                right=false
                left=true
            }
            else{
                positionX += speed
            }
        }
        else{
            if (positionX-speed < 0 ){
                positionX = 0
                right=true
                left=false
            }
            else{
                positionX -= speed
            }
        }

    }
    fun enemy(score: Int){
        numero= (0..enemies.lastIndex).random()
        bitmap= enemies[numero]
        type= numero
        when(type){
            0,4->{
                speed=10
            }
            1,2->{speed=20}
            3->{speed=20}

        }
        when(score){
            in 1500..2500-> speed+=10
            in 2600..3500-> speed+=20
            in 3600..5000-> speed+=30
            in 5100..10000-> speed+=40
            in 10100..25000->speed+=50
            in 25100..200000-> speed+=60
        }
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(),false)
    }
}

