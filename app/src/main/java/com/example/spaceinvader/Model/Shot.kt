package com.example.spaceinvader.Model

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.RectF
import com.example.spaceinvader.R
class Shot(context: Context, screenX: Int, screenY: Int)  {
    var bitmap: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.laser)
    val vertical= screenY
    val horizontal= screenX
    var width = screenX / 15f
    var height = screenY / 18f
    var positionX = 0f
    var positionY= screenY-500f
    var speed = 100
    var colision= false
    val hitbox= RectF()

    init{
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(),false)
    }

    fun updateShot(numX: Float, superShot: Int){
        if (superShot==5){
            height= 500f
            width=250f
            bitmap= Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(),false)
        }
        else{
            height= vertical/20f
            width=horizontal/20f
            bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(),false)
        }
        positionX= numX
        if(colision){
            positionY= vertical-500f
            colision=false
        }
        else{
            positionY-=speed
        }
    }
}