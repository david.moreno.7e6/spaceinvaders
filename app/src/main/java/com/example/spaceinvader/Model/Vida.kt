package com.example.spaceinvader.Model

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.RectF
import com.example.spaceinvader.R

class Vida(context: Context, screenX: Int, screenY: Int) {
    var bitmap: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.vida)
    val width = screenX / 15f
    val height = screenY / 22f
    val horizontal= screenX
    var positionX= (0..screenX-500).random()
    var active= false
    val hitbox= RectF()
    var number=1
    init{
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(),false)
    }
    fun random(){
        positionX= (0..horizontal).random()
    }
}