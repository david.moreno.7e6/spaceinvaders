package com.example.spaceinvader.View

import android.media.MediaPlayer
import android.media.SoundPool
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.spaceinvader.R
import com.example.spaceinvader.databinding.FragmentMenuBinding


class MenuFragment : Fragment() {

lateinit var binding: FragmentMenuBinding
    val soundPool: SoundPool = SoundPool.Builder().setMaxStreams(5).build()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding= FragmentMenuBinding.inflate(layoutInflater)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val click = soundPool.load(context, R.raw.click, 0)
        val mediaPlayer: MediaPlayer? = MediaPlayer.create(context, R.raw.menusong)
        mediaPlayer?.start()
        mediaPlayer?.setOnCompletionListener {
            mediaPlayer.start()
        }
        binding.start.setOnClickListener {
            mediaPlayer?.stop()
            playSound(click)
            findNavController().navigate(R.id.action_menuFragment_to_gameFragment)
        }
        binding.help.setOnClickListener {
            mediaPlayer?.stop()
            playSound(click)
            findNavController().navigate(R.id.action_menuFragment_to_helpFragment)
        }
        binding.ranking.setOnClickListener {
            mediaPlayer?.stop()
            playSound(click)
            findNavController().navigate(R.id.action_menuFragment_to_rankingFragment)
        }
    }
    fun playSound(id: Int){
        soundPool.play(id, 1f, 1f, 0, 0, 1f)
    }

}