package com.example.spaceinvader.View

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.media.MediaPlayer
import android.media.SoundPool
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.spaceinvader.R
import com.example.spaceinvader.databinding.FragmentResultBinding
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import kotlin.io.path.Path
import kotlin.io.path.appendText
import kotlin.io.path.readLines

class ResultFragment : Fragment() {
    lateinit var binding: FragmentResultBinding
    val soundPool: SoundPool = SoundPool.Builder().setMaxStreams(5).build()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding= FragmentResultBinding.inflate(layoutInflater)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mediaPlayer: MediaPlayer? = MediaPlayer.create(context, R.raw.resultsong)
        mediaPlayer?.start()
        mediaPlayer?.setOnCompletionListener {
            mediaPlayer.start()
        }
        val click = soundPool.load(context, R.raw.click, 0)
        binding.scorevalue.text= arguments?.getInt("score").toString()
        binding.timevalue.text= arguments?.getString("segons").toString() + "s"
        val score= arguments?.getInt("score")
        val segundos=arguments?.getString("segons")
        var empty=false
        if (fileExists("ranking")==false){
            empty=true
            kotlin.io.path.createTempFile("ranking")
        }
        writeFile(arguments?.getInt("score").toString())
        val scoreList= readFile()
        scoreList.sortDescending()
        println(scoreList)
        if (scoreList[0]< arguments?.getInt("score")!! || empty){
            binding.record.visibility= View.VISIBLE
        }
        else{
            binding.record.visibility= View.INVISIBLE
        }
        binding.back.setOnClickListener {
            mediaPlayer?.stop()
            playSound(click)
            findNavController().navigate(R.id.action_resultFragment_to_menuFragment)
        }
        binding.share.setOnClickListener {
            playSound(click)
            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, ("He conseguido $score puntos en $segundos segundos ! Atrévete a superarme !").uppercase())
                type = "text/plain"
            }

            val shareIntent = Intent.createChooser(sendIntent, null)
            startActivity(shareIntent)
        }

    }
    fun fileExists(file:String): Boolean{
        val files = requireActivity().fileList()
        val exists = files.find { it == file }
        if(exists!=null) return true
        return false
    }


    fun readFile(): MutableList<Int> {
        val scoreList= mutableListOf<Int>()
        val file = InputStreamReader(requireActivity().openFileInput("ranking"))
        val br = BufferedReader(file)
        val lines = br.readLines()
        for (i in lines){
            if (i!=""){
                scoreList.add(i.toInt())
            }
        }
        return scoreList
    }
    fun writeFile(score: String){
        val file = OutputStreamWriter(requireActivity().openFileOutput("ranking", Activity.MODE_APPEND))
        file.appendLine(score)
        file.flush()
        file.close()
    }
    fun playSound(id: Int){
        soundPool.play(id, 1f, 1f, 0, 0, 1f)
    }


}