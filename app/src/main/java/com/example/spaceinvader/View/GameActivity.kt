package com.example.spaceinvader.View

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.spaceinvader.R
import com.example.spaceinvader.databinding.ActivityGameBinding

class GameActivity : AppCompatActivity() {
    lateinit var binding: ActivityGameBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityGameBinding.inflate(layoutInflater)
        setContentView(binding.root)

    }
}