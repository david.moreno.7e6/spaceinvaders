package com.example.spaceinvader.View

import android.content.Context
import android.graphics.*
import android.media.MediaPlayer
import android.media.SoundPool
import android.os.Bundle
import android.view.MotionEvent
import android.view.SurfaceView
import androidx.fragment.app.findFragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.spaceinvader.Model.*
import com.example.spaceinvader.R
import kotlinx.coroutines.*
import kotlin.properties.Delegates
import kotlin.system.measureTimeMillis


class GameView(context: Context, private val size: Point, val fragment: GameFragment) : SurfaceView(context){

    var canvas: Canvas = Canvas()
    val paint: Paint = Paint()
    var enemy: Enemy
    var player: Player
    var vida: Vida
    var shot: Shot
    var score by Delegates.notNull<Int>()
    var playing= true
    var shotCharge= 0
    var enemyShotCharge=0
    var superShoot=0
    var shotPosition = 0f
    var enemyShotPosition= 0f
    var enemyShot: EnemyShot
    val background= BitmapFactory.decodeResource(context.resources, R.drawable.space)
    val vidas= BitmapFactory.decodeResource(context.resources, R.drawable.corazon)
    lateinit var energia: Bitmap
    var mediaPlayer: MediaPlayer? = MediaPlayer.create(context, R.raw.juego)
    val soundPool = SoundPool.Builder().setMaxStreams(5).build()
    val playerShot = soundPool.load(context, R.raw.laser, 0)
    val playerSuperShot = soundPool.load(context, R.raw.superlaser, 0)
    val maxPower=  soundPool.load(context, R.raw.eructopower, 0)
    val enemyDeath= soundPool.load(context, R.raw.enemydeath, 0)
    val lose= soundPool.load(context, R.raw.lose, 0)
    val fail= soundPool.load(context, R.raw.fail, 0)
    val playerHitted= soundPool.load(context, R.raw.playerhit, 0)
    var portalShow=0
    var enemyDeathPosition=0
    val portal=BitmapFactory.decodeResource(context.resources, R.drawable.portal)
    var time=0

    init{
        enemy= Enemy(context, size.x, size.y)
        player= Player(context,size.x,size.y)
        shot = Shot(context,size.x,size.y)
        score=0
        enemyShot= EnemyShot(context,size.x, enemy.positionX, enemy.type,score)
        vida= Vida(context,size.x,size.y)
        startGame()
    }

    fun startGame(){
            CoroutineScope(Dispatchers.Main).launch{
                while(playing){
                    time++
                    mediaPlayer?.start()
                    mediaPlayer?.setOnCompletionListener {
                        mediaPlayer?.start()
                    }
                    shotCharge++
                    enemyShotCharge++
                    draw()
                    update()
                    if (shotCharge==20){
                        if (superShoot==5){
                            playSound(playerSuperShot)
                        }
                        else{
                            playSound(playerShot)
                        }
                        shotPosition= player.positionX+100f
                    }
                    if (enemyShotCharge==10){
                        enemyShotPosition= enemy.positionX+100f
                    }
                    delay(20)
                }
                mediaPlayer?.stop()
                playSound(lose)
                val parametros = Bundle()
                parametros.putString("segons", (time*0.5/10).toInt().toString())
                parametros.putInt("score", score)
                val navController = fragment.findNavController()
                navController.navigate(R.id.action_gameFragment_to_resultFragment, parametros)

            }
        }


    fun draw(){
        if (holder.surface.isValid) {
            canvas = holder.lockCanvas()
            when(score){
                in 0..1500-> canvas.drawBitmap(BitmapFactory.decodeResource(context.resources, R.drawable.space),0f,0f, null)
                in 1500..2500-> canvas.drawBitmap(BitmapFactory.decodeResource(context.resources, R.drawable.space7),0f,0f, null)
                in 2600..3500-> canvas.drawBitmap(BitmapFactory.decodeResource(context.resources, R.drawable.space5),0f,0f, null)
                in 3600..5000-> canvas.drawBitmap(BitmapFactory.decodeResource(context.resources, R.drawable.space2),0f,0f, null)
                in 5100..10000-> canvas.drawBitmap(BitmapFactory.decodeResource(context.resources, R.drawable.space4),0f,0f, null)
                in 10100..25000->canvas.drawBitmap(BitmapFactory.decodeResource(context.resources, R.drawable.space6),0f,0f, null)
                in 25100..200000-> canvas.drawBitmap(BitmapFactory.decodeResource(context.resources, R.drawable.space3),0f,0f, null)
            }
            paint.color = Color.GREEN
            paint.textSize = 60f
            when(score){
                in 0..1500-> canvas.drawText("Level: loser", (size.x - paint.descent()), 130f, paint)
                in 1500..2500-> canvas.drawText("Level: still loser", (size.x - paint.descent()), 130f, paint)
                in 2600..3500-> canvas.drawText("Level: noob", (size.x - paint.descent()), 130f, paint)
                in 3600..5000-> canvas.drawText("Level: easy", (size.x - paint.descent()), 130f, paint)
                in 5100..10000-> canvas.drawText("Level: not bad", (size.x - paint.descent()), 130f, paint)
                in 10100..25000->canvas.drawText("Level: Good boy", (size.x - paint.descent()), 130f, paint)
                in 25100..200000-> canvas.drawText("Level: Max", (size.x - paint.descent()), 130f, paint)
            }
            when(superShoot){
                0,1->{
                    energia= BitmapFactory.decodeResource(context.resources, R.drawable.energia1)
                    canvas.drawBitmap( Bitmap.createScaledBitmap(energia, size.y/2-100, 150,false),(size.x/2 - (size.y/2-100)/2).toFloat(),size.y-200f,paint)
                }
                2->{
                    energia= BitmapFactory.decodeResource(context.resources, R.drawable.energia2)
                    canvas.drawBitmap( Bitmap.createScaledBitmap(energia, size.y/2, 150,false),80f,size.y-200f,paint)
                }
                3->{
                    energia= BitmapFactory.decodeResource(context.resources, R.drawable.energia3)
                    canvas.drawBitmap( Bitmap.createScaledBitmap(energia, size.y/2, 150,false),80f,size.y-200f,paint)
                }
                4->{
                    energia= BitmapFactory.decodeResource(context.resources, R.drawable.energia4)
                    canvas.drawBitmap( Bitmap.createScaledBitmap(energia, size.y/2, 150,false),80f,size.y-200f,paint)
                }
                5->{
                    energia= BitmapFactory.decodeResource(context.resources, R.drawable.energia5)
                    canvas.drawBitmap( Bitmap.createScaledBitmap(energia, size.y/2, 150,false),80f,size.y-200f,paint)
                }
            }
            if (vida.active){
                canvas.drawBitmap(vida.bitmap,vida.positionX.toFloat(), size.y.toFloat()-500f,paint)
            }
            paint.textAlign = Paint.Align.RIGHT
            var position=0f
            for (i in 1..player.lifes){
                canvas.drawBitmap( Bitmap.createScaledBitmap(vidas, 100, 100,false),position,20f,paint)
                position+=100f
            }
            canvas.drawText("Score: $score", (size.x - paint.descent()), 55f, paint)
            canvas.drawBitmap(player.bitmap, player.positionX.toFloat(), size.y.toFloat()-500f, paint)
            if(!RectF.intersects(enemy.hitbox,shot.hitbox)){
                canvas.drawBitmap(enemy.bitmap, enemy.positionX.toFloat(),180f, paint)
                if (shotCharge>=20){
                    canvas.drawBitmap(shot.bitmap, shotPosition, shot.positionY, paint)
                }
            }
            else{
                while (portalShow!=10){
                    portalShow++
                    canvas.drawBitmap( Bitmap.createScaledBitmap(portal, enemy.width.toInt(), enemy.height.toInt(),false),enemyDeathPosition.toFloat(),180f,paint)
                }
                portalShow=0
            }
            if (enemyShotCharge>=10){
                canvas.drawBitmap(enemyShot.bitmap, enemyShotPosition, enemyShot.positionY, paint)
            }
            holder.unlockCanvasAndPost(canvas)
        }
    }


    fun update(){
        enemy.hitbox.top = 180f
        enemy.hitbox.bottom = enemy.hitbox.top+enemy.height
        enemy.hitbox.left = enemy.positionX.toFloat()+10f
        enemy.hitbox.right = enemy.hitbox.left+enemy.width-20f
        shot.hitbox.top = shot.positionY
        shot.hitbox.bottom = shot.hitbox.top+shot.height
        shot.hitbox.left = shotPosition
        shot.hitbox.right = shot.hitbox.left+shot.width
        enemyShot.hitbox.top= enemyShot.positionY
        enemyShot.hitbox.bottom= enemyShot.hitbox.top+ enemyShot.height
        enemyShot.hitbox.left= enemyShotPosition
        enemyShot.hitbox.right= enemyShot.hitbox.left+ enemyShot.width
        player.hitbox.top= size.y-500f
        player.hitbox.bottom= player.hitbox.top + player.height
        player.hitbox.left= player.positionX.toFloat()
        player.hitbox.right= player.hitbox.left+ player.width
        if (vida.active){
            vida.hitbox.left=vida.positionX.toFloat()
            vida.hitbox.right=vida.hitbox.left + vida.width
            vida.hitbox.top= size.y-500f
            vida.hitbox.bottom= vida.hitbox.top + vida.height
            if (RectF.intersects(player.hitbox, vida.hitbox)){
                vida.random()
                player.lifes+=1
                vida.active=false
                vida.number=1
            }
        }
        else{
            if (vida.number==1){
                vida.number= 0
                GlobalScope.launch {
                    delay(30000)
                    vida.active=true
                }
            }

        }
        enemy.updateEnemy()
        player.updatePlayer()
        if (shotCharge>=20){
            shot.updateShot(shotPosition,superShoot)
            if(RectF.intersects(enemy.hitbox,shot.hitbox)){
                playSound(enemyDeath)
                enemyDeathPosition= enemy.positionX
                if (superShoot==5){
                    superShoot=0
                }
                superShoot++
                if (superShoot==5){
                    playSound(maxPower)
                }
                enemy.enemy(score)
                enemyShot= EnemyShot(context, size.x, size.y,enemy.type,score)
                score+=100
                shot.positionY=size.y-500f
                shotCharge=0
            }
            if (shot.positionY<=0 ){
                if (superShoot==5){
                    playSound(fail)
                    superShoot=0
                }
                shot.positionY=size.y-500f
                shotCharge= 0
            }
        }
        if(enemyShotCharge>=10 ){
            enemyShot.updateShot(enemyShotPosition)
            if (RectF.intersects(player.hitbox, enemyShot.hitbox)){
                if (player.lifes>1){
                    playSound(playerHitted)
                }
                player.lifes-=1
                if (player.lifes==0){
                    playing=false
                }
                enemyShotCharge=0
                enemyShot.positionY= 180f
            }
            if (enemyShot.positionY>=size.y){
                enemyShot.positionY= 180f
                enemyShotCharge=0
            }
        }

    }
    fun playSound(id: Int){
        soundPool.play(id, 1f, 1f, 0, 0, 1f)
    }
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event != null) {
            when(event.action){
                // Aquí capturem els events i el codi que volem executar per cadascún
                MotionEvent.ACTION_DOWN, MotionEvent.ACTION_MOVE -> {
                    // Modifiquem la velocitat del jugador perquè es mogui?
                    if(event.x>player.positionX){
                        player.speed = 10
                    }
                    else if(event.x< player.positionX){
                        player.speed = -10
                    }
                }
            }
        }
        return true
    }
}