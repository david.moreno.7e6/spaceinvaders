package com.example.spaceinvader.View

import android.media.SoundPool
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.spaceinvader.R
import com.example.spaceinvader.databinding.FragmentRankingBinding
import java.io.BufferedReader
import java.io.InputStreamReader

class RankingFragment : Fragment() {
lateinit var binding: FragmentRankingBinding
    val soundPool: SoundPool = SoundPool.Builder().setMaxStreams(5).build()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding= FragmentRankingBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (fileExists("ranking")!=false){
            readFile()
        }
        val click = soundPool.load(context, R.raw.click, 0)
        binding.back.setOnClickListener {
            playSound(click)
            findNavController().navigate(R.id.action_rankingFragment_to_menuFragment)
        }
    }
    fun readFile(){
        var scores = mutableListOf<String>()
        val file = InputStreamReader(requireActivity().openFileInput("ranking"))
        val br = BufferedReader(file)
        var scoreboard=""
        var scoreList = br.readLines()
        for (score in scoreList){
            if (score != ""){
                scores.add(score)
            }
        }
        scores.sortByDescending { it.toInt()}
        var number=0
        for (score in scores){
            number++
            if (number>10){
                break
            }
            scoreboard += "$score\n"
        }
        binding.score.text= scoreboard
    }
    fun playSound(id: Int){
        soundPool.play(id, 1f, 1f, 0, 0, 1f)
    }
    fun fileExists(file:String): Boolean{
        val files = requireActivity().fileList()
        val exists = files.find { it == file }
        if(exists!=null) return true
        return false
    }

}