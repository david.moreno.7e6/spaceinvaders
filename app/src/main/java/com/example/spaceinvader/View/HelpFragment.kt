package com.example.spaceinvader.View

import android.media.SoundPool
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.spaceinvader.R
import com.example.spaceinvader.databinding.FragmentHelpBinding


class HelpFragment : Fragment() {
    lateinit var binding: FragmentHelpBinding
    val soundPool: SoundPool = SoundPool.Builder().setMaxStreams(5).build()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding= FragmentHelpBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val click = soundPool.load(context, R.raw.click, 0)
        binding.back.setOnClickListener {
            playSound(click)
            findNavController().navigate(R.id.action_helpFragment_to_menuFragment)
        }
    }
    fun playSound(id: Int){
        soundPool.play(id, 1f, 1f, 0, 0, 1f)
    }

}